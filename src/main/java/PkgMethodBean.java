import java.util.Objects;

public class PkgMethodBean {
    private String pkgName;
    private String methodName;

    public PkgMethodBean(String pkgName,String methodName){
        this.pkgName = pkgName;
        this.methodName = methodName;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PkgMethodBean that = (PkgMethodBean) o;
        return pkgName.equals(that.pkgName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkgName);
    }
}
